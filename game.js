const allButtons = document.querySelectorAll('.btn');
const startButton = document.querySelector('#start');
const title = document.querySelector('#level-title');
const titleSpan = title.firstElementChild;
const message = document.querySelector('#message');
const bodyElement = document.querySelector('body');
const timerDisplay = document.querySelector('#timer');

const colorArray = ['green', 'red', 'yellow', 'blue'];
const Game_Over_Message = 'GAME OVER';
const Computer_Turn = 'Computer turn';
const Player_Turn = 'Player turn';
const Pressed = 'pressed';
const Display_None = 'display-none';

let computerColorArray = [];
let playerColorArray = [];
let colorIndex = 0;
let levelNumber = 0;
let timerId;

/* start of  Game start */
function initiateGame() {
	toggleDisplay();
	bodyElement.style.backgroundColor = '#011f3f';

	startGameHandler();
	message.classList.remove('invisible');
}
function startGameHandler() {
	levelNumber++;
	titleSpan.innerHTML = levelNumber;
	computerColorArray = [];
	setTimeout(() => {
		message.innerHTML = Computer_Turn;
	}, 500);
	computerPickRandomColor(levelNumber);
	removeListenerToColorButton();
	setTimeout(() => playComputerColorArray(), 1000);
	resetPlayerParameters();
}
/* end of  Game start */

/* start of Player functions */

function clickHandler() {
	playerPickColor(this.id);
}

function playerPickColor(selectedColor) {
	playerColorArray.push(selectedColor);
	compareColor(selectedColor);
}

function compareColor(color) {
	if (playerColorArray[colorIndex] === computerColorArray[colorIndex]) {
		animateButton(color);
		if (colorIndex < computerColorArray.length - 1) {
			colorIndex++;

			stopTimer(timerId);
			startTimer();
		} else {
			stopTimer(timerId);
			startGameHandler();
		}
	} else {
		gameOver();
	}
}

function wrongColor() {
	message.innerHTML = Game_Over_Message;
	bodyElement.style.backgroundColor = 'red';
}

function resetPlayerParameters() {
	colorIndex = 0;
	playerColorArray = [];
}

function gameOver() {
	levelNumber = 0;
	stopTimer(timerId);
	wrongColor();
	addSound();
	removeListenerToColorButton();
	resetPlayerParameters();
	setTimeout(() => ready(), 2000);
}

/* end of Player functions */

/* start of Computer functions */

function computerPickRandomColor(loopNumber) {
	for (let i = 0; i < loopNumber; i++) {
		let randomNumber = Math.floor(Math.random() * colorArray.length);
		let aiColor = colorArray[randomNumber];
		computerColorArray.push(aiColor);
	}
	// console.log(computerColorArray);
}
function playComputerColorArray() {
	let i = 0;
	function myLoop() {
		setTimeout(function () {
			animateButton(computerColorArray[i]);
			i++;
			if (i < computerColorArray.length) {
				myLoop();
			} else {
				setTimeout(() => {
					message.innerHTML = Player_Turn;
					addListenerToColorButton();
					startTimer();
				}, 500);
			}
		}, 500);
	}
	myLoop();
}
/* end of Computer functions */

/* start of Common functions */
function animateButton(color) {
	const selectedButton = document.querySelector('#' + color);
	selectedButton.classList.add(Pressed);
	setTimeout(function () {
		selectedButton.classList.remove(Pressed);
	}, 100);
	addSound(color);
}

function addSound(type) {
	switch (type) {
		case 'green':
			var greenAudio = new Audio('sounds/green.mp3');
			greenAudio.play();
			break;
		case 'red':
			var redAudio = new Audio('sounds/red.mp3');
			redAudio.play();
			break;
		case 'yellow':
			var yellowAudio = new Audio('sounds/yellow.mp3');
			yellowAudio.play();
			break;
		case 'blue':
			var blueAudio = new Audio('sounds/blue.mp3');
			blueAudio.play();
			break;

		default:
			var wrong = new Audio('sounds/wrong.mp3');
			wrong.play();
			break;
	}
}
/* end of Common functions */

/* start Timer functions */

function startTimer() {
	timerDisplay.classList.remove(Display_None);
	let time = 5;

	timerDisplay.innerHTML = time;
	timerId = setInterval(() => {
		if (time < 1) {
			stopTimer(timerId);

			gameOver();
		} else {
			time--;
			timerDisplay.innerHTML = time;
		}
	}, 1000);
}

function stopTimer(id) {
	clearInterval(id);
	timerDisplay.classList.add(Display_None);
}
/* stop Timer functions */

function addListenerToColorButton() {
	allButtons.forEach((button) => {
		button.addEventListener('click', clickHandler);
	});
}
function removeListenerToColorButton() {
	allButtons.forEach((button) => {
		button.removeEventListener('click', clickHandler);
	});
}

function toggleDisplay() {
	startButton.classList.toggle(Display_None);
	title.classList.toggle(Display_None);
}

function ready() {
	toggleDisplay();
	computerColorArray = [];
	startButton.addEventListener('click', initiateGame);
}

ready();
